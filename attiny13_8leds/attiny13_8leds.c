#include <avr/io.h>

int main(void)
{
  DDRB = (1<<DDB0) | (1<<DDB4);
  PORTB = 0;

  int leds[] = {1,0,0,1,0,0,0,1};

  for (int i=0;i<8;i++)
    {
      PORTB = leds[i] == 1 ? (1<<PB0) : 0; // data      
      PORTB |= (1<<PB4); // shift
    }
  return 0;
}
