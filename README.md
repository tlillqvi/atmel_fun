# README #

Atmel microcontroller programming 4 fun. Small snipets of code etc to get the atmel running.

## What's needed ##

Electronics: ATtiny13, breadboard, jumper wires leds etc.

Programmer HW: USBASP http://www.fischl.de/usbasp/

Programming SW: avrdude

Linux Debian with avr-gcc etc.

## Quick start ##

Install software. Tested on Debian 8

    sudo apt-get install gcc-avr avr-libc avrdude
    sudo usermod -a -G plugdev $USER # After this log out and in again to join the plugdev group
    sudo echo > /etc/udev/rules.d/50-usbasp-plugdev-grp.rules '
    ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="05dc", MODE="0666", GROUP="plugdev"'

Plug in the programmer and the atmel into the programmer

    cd <some-subdir-with-a-Makefile>
    make
    make prog
