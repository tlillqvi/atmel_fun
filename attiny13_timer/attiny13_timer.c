/**
 *
 *  TIM0_OVF interrupt @ 9.6Mhz (CKSEL 10) / 1024 ( CS0X 101)
 *   => 1s / 9600000 * (256 * 1024)
 *   => 27.3ms between interrupts. (48on/10s => 96onoff/10s = 10onoff/s => 100ms between interrupts
 *
 */

#include <avr/io.h>
#include <avr/interrupt.h>

char i = 0;
// Interrupt Service Routine
// org 0x3 TIM0_OVF Timer/Counter Overflow
ISR(TIM0_OVF_vect)
{
    // Slow things down a bit
    i++;
    i = i % 8;
    if(i==0)
      PORTB ^= (1<<PB4)|(1<<PB3); // PB3, PB4 toggle
}

int main(void)
{
    cli();   //Clears global interrupt flag

    DDRB = (1<<DDB4)|(1<<DDB3); // Set PB3 and PB4 as output. Pins 2 & 3 on attiny13 8-pin dip.

    PORTB = (1<<PB4); // PB4 on, PB3 off

    // page 73. 1 0 1  => 1024 prescaler
    TCCR0B |= (1<<CS02) | (0<<CS01) | (1<<CS00);

    TIMSK0 |= (1<<TOIE0);   // Enable timer interrupt
    sei();                  // Enables interrupts

    while(1);
}
