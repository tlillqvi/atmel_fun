// The maximal possible delay is 262.14 ms / F_CPU in MHz.

#include <avr/io.h>
#include <util/delay.h>

int main(void) {
    DDRB = (1<<DDB3); // PB3 as output
    PORTB = (1<<PB3); // PB3 high
    while(1)
    {
        _delay_ms(250);
        PORTB ^= (1<<PB3); // toggle w/ xor
    }
    return 0;
}
