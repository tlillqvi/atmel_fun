/**
 *
 *  INT0 interrupt @ 9.6Mhz (CKSEL 10) / 1024 ( CS0X 101)
 *   => 1s / 9600000 * (256 * 1024)
 *   => 27.3ms between interrupts.
 *
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

// Interrupt Service Routine
ISR(INT0_vect)
{
  
  PORTB ^= (1<<PB4);
  _delay_ms(500);
  PORTB ^= (1<<PB4)|(1<<PB3);
  _delay_ms(500);
  PORTB ^= (1<<PB4)|(1<<PB3);
  _delay_ms(500);
  PORTB ^= (1<<PB3);
  _delay_ms(500);

  PORTB = 0;
}

int main(void)
{
    // int - Enable external interrupts int0
    GIMSK |= _BV (INT0);
    // Set falling edge.
    MCUCR =  _BV (ISC01); // Add for rising edge | _BV (ISC00);

    sei(); // set enable interrupts

    DDRB = (1<<DDB4)|(1<<DDB3);// output on pins PB3 and PB4
    PORTB = 0;

    while(1);
}
