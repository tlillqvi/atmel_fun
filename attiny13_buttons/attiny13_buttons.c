#include <avr/io.h>
#include <avr/interrupt.h>

// pin-change interrupt routine
ISR (PCINT0_vect, ISR_NOBLOCK)
{
  PORTB ^= 1<<PB3; // toggle
}

// edge triggered interrupt
ISR (INT0_vect, ISR_NOBLOCK)
{
  PORTB ^= 1<<PB4; // toggle
}

int main(void) {
  PCMSK |= (1<<PCINT0);  // pin change mask
  GIMSK |= (1<<PCIE);    // global int mask
  GIMSK |= _BV (INT0);   // global int mask
  MCUCR = _BV (ISC01) | _BV(ISC00);
  sei();

  DDRB = (1<<DDB4) | (1<<DDB3);
  while(1);
}
