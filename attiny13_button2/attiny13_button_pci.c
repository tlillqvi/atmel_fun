/**
 *
 *  INT0 interrupt @ 9.6Mhz (CKSEL 10) / 1024 ( CS0X 101)
 *   => 1s / 9600000 * (256 * 1024)
 *   => 27.3ms between interrupts.
 *
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

// Interrupt Service Routine
ISR(PCINT0_vect)
{
  
  PORTB ^= (1<<PB4);
  _delay_ms(500);
  PORTB ^= (1<<PB4)|(1<<PB3);
  _delay_ms(500);
  PORTB ^= (1<<PB4)|(1<<PB3);
  _delay_ms(500);
  PORTB ^= (1<<PB3);
  _delay_ms(500);

  PORTB = 0;
}

int main(void)
{
  // pin change mask: listen to the designated pin of port B. (Choose
  // PCINT0,PCINT1, etc...)
  PCMSK |= (1<<PCINT0);

  // enable Pin-Change interrupt (PCINT)
  GIMSK |= (1<<PCIE);
  
  sei();
  DDRB = (1<<DDB4) | (1<<DDB3);
  PORTB = 0;
  while(1);
}
